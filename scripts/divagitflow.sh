function create(){
    echo_info "Actual status..."
    git status
    echo_info "Moving into master..."
    git checkout master 
    echo_info "Updating master..."
    git pull origin master
    echo_info "Creating feature branch..."
    git checkout -b $1
    echo_success "Feature branch #$1 created 🙏🙏🙏"
}

function update(){
    echo_info "Actual status..."
    git status
    echo_info "Moving into master..."
    git checkout master 
    echo_info "Updating master..."
    git pull origin master
    echo_info "Moving into feature branch #$1..."
    git checkout $1
    echo_info "Merging master into #$1..."
    git merge master
    echo_success "FB#$1 updated with master 😺 😸 😹"
}

function stage(){
    echo_info "Actual status..."
    git status
    echo_info "Moving into staging..."
    git checkout staging
    echo_info "Updating staging..."
    git pull origin staging
    echo_info "Merging #$1 into staging..."
    git merge $1
    echo_success "Merged #$1 into staging 😎😎😎"
}