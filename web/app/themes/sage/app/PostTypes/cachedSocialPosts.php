<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('init', 'type_post_social_post');

function type_post_social_post()
{
    $labels = array(
        'name' => _x('Posts Cacheados', 'post type general name'),
        'singular_name' => _x('Post Cacheado', 'post type singular name')
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'supports' => array('title', 'editor', 'thumbnail'),
        'menu_icon' => 'dashicons-rss',
        'rewrite' => array(
            'slug' => 'cached-social-posts',
        ),
    );

    register_post_type('cached-social-post', $args);
    flush_rewrite_rules();
}

add_action('carbon_fields_register_fields', 'attach_social_cached_posts_fields', 10);


function attach_social_cached_posts_fields()
{
    Container::make_post_meta('Post')
        ->set_context('carbon_fields_after_title')
        ->where('post_type', '=', 'cached-social-post')
        ->add_fields(array(
            Field::make_select('cached-social-post-social-media', __('Rede Social'))
                ->set_options(array(
                    "instagram" => "Instagram",
                    "facebook" => "Facebook",
                    "twitter" => "Twitter"
                )),
            Field::make('text', 'cached-social-post-external-id', 'ID Externo'),
            Field::make('text', 'cached-social-post-caption', 'Legenda'),
            Field::make('text', 'cached-social-post-media-url', 'URL da Mídia'),
            Field::make_select('cached-social-post-media-type', 'Tipo de Mídia')
                ->set_options(array(
                    "IMAGE" => "Imagem",
                    "VIDEO" => "Vídeo",
                    "CAROUSEL_ALBUM" => "Carrossel",
                    "STATUS" => "Status"
                )),
            Field::make('text', 'cached-social-post-permalink', 'Link do Post'),
            Field::make('text', 'cached-social-post-timestamp', 'Criado em'),
            Field::make('text', 'cached-social-post-thumbnail-url', 'URL da Thumbnail'),
        ));
}
