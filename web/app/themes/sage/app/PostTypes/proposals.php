<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('init', 'type_post_proposals');

function type_post_proposals()
{
    $labels = array(
        'name' => _x('Propostas', 'post type general name'),
        'singular_name' => _x('Proposta', 'post type singular name')
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'supports' => array('title', 'editor', 'thumbnail'),
        'menu_icon' => 'dashicons-format-aside',
        'rewrite' => array(
            'slug' => 'propostas',
        ),
    );

    register_post_type('proposal', $args);
    flush_rewrite_rules();
}

add_action('carbon_fields_register_fields', 'attach_social_proposals_fields', 10);


function attach_social_proposals_fields()
{
    Container::make_post_meta('Proposta')
        ->set_context('carbon_fields_after_title')
        ->where('post_type', '=', 'proposal')
        ->add_fields(array(
            Field::make('text', 'proposal-title', 'Título da proposta'),
            Field::make('rich_text', 'proposal-text', 'Texto da proposta'),
            Field::make_image('proposal-image', 'Imagem ilustrativa'),
            Field::make_text('proposal-search-matching')
                ->set_default_value('Proposta')
        ));
}
