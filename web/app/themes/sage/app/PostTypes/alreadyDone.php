<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('init', 'type_post_already_done');

function type_post_already_done()
{
    $labels = array(
        'name' => _x('DivaFez', 'post type general name'),
        'singular_name' => _x('DivaFez', 'post type singular name')
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'supports' => array('title', 'editor', 'thumbnail'),
        'menu_icon' => 'dashicons-yes-alt',
        'rewrite' => array(
            'slug' => 'divafez',
        ),
    );

    register_post_type('already-done', $args);
    flush_rewrite_rules();
}

add_action('carbon_fields_register_fields', 'attach_social_already_done_fields', 10);

function attach_social_already_done_fields()
{
    Container::make_post_meta('DivaFez')
        ->set_context('carbon_fields_after_title')
        ->where('post_type', '=', 'already-done')
        ->add_fields(array(
            Field::make('text', 'already-done-title', 'Título do DivaFez'),
            Field::make('rich_text', 'already-done-text', 'Texto do DivaFez'),
            Field::make_image('already-done-image', 'Imagem ilustrativa'),
            Field::make_file('already-done-file', 'Arquivo PDF do DivaFez'),
            Field::make_hidden('already-done-search-matching')
                ->set_default_value('#DivaFez')
        ));
}
