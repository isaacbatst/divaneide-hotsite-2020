<?php

require(dirname(__FILE__) . '/cachedSocialPosts.php');
require(dirname(__FILE__) . '/thematicWeeks.php');
require(dirname(__FILE__) . '/proposals.php');
require(dirname(__FILE__) . '/alreadyDone.php');

add_action('init', 'register_taxonomies', 10);

function register_taxonomies()
{
    register_taxonomy(
        'page-category',
        ['page, already-done', 'proposal'],
        array(
            'label' => 'Categorias das páginas',
            'singular_label' => 'Categoria da página',
            'hierarchical' => true,
        )
    );

    register_taxonomy_for_object_type('page-category', 'page');
    register_taxonomy_for_object_type('page-category', 'already-done');
    register_taxonomy_for_object_type('page-category', 'proposal');
}
