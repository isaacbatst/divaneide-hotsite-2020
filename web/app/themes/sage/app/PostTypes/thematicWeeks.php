<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('init', 'type_post_thematic_week');

function type_post_thematic_week()
{
    $labels = array(
        'name' => _x('Semanas Temáticas ', 'post type general name'),
        'singular_name' => _x('Semana Temática', 'post type singular name')
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'supports' => array('title', 'editor', 'thumbnail'),
        'menu_icon' => 'dashicons-admin-appearance',
        'rewrite' => array(
            'slug' => 'thematic-weeks',
        ),
    );

    register_post_type('thematic-week', $args);
    flush_rewrite_rules();
}

add_action('carbon_fields_register_fields', 'attach_social_thematic_weeks_fields', 10);


function attach_social_thematic_weeks_fields()
{
    Container::make_post_meta('Semana temática')
        ->set_context('carbon_fields_after_title')
        ->where('post_type', '=', 'thematic-week')
        ->add_fields(array(
            Field::make('text', 'thematic-week-title', 'Título da Semana'),
            Field::make_color('thematic-week-primary-color', 'Cor primária da Semana')
                ->set_palette(['#2CB6E7', '#5AD38E', '#FFCA00', '#FA6666', '#8A00B8', '#FF0044'])
                ->set_alpha_enabled(),
            Field::make('rich_text', 'thematic-week-text', 'Texto da Semana'),
        ));
}
