<?php

namespace App\Adapters;

use WP_Post;

Class InternalPostsAdapter {
    static public function adaptTemplateInterno(WP_Post $post) {
        return [
            'ID' => $post->ID,
            'permalink' => get_permalink($post->ID),
            'post_date' => $post->post_date,
            'post_title' => carbon_get_post_meta($post->ID, 'template-interno-title'),
            'post_text' => carbon_get_post_meta($post->ID, 'template-interno-text'),
            'post_image' => get_post(carbon_get_post_meta($post->ID, 'template-interno-image'))->guid,
            'post_type' => $post->post_type,
        ];
    }

    static public function adaptProposal(WP_Post $post) {
        return [
            'ID' => $post->ID,
            'permalink' => get_permalink($post->ID),
            'post_date' => $post->post_date,
            'post_title' => carbon_get_post_meta($post->ID, 'proposal-title'),
            'post_text' => carbon_get_post_meta($post->ID, 'proposal-text'),
            'post_image' => get_post(carbon_get_post_meta($post->ID, 'proposal-image'))->guid  ?? '',
            'post_type' => $post->post_type
        ];
    }

    static public function adaptAlreadyDone(WP_Post $post) {
        return [
            'ID' => $post->ID,
            'permalink' => get_permalink($post->ID),
            'post_date' => $post->post_date,
            'post_title' => carbon_get_post_meta($post->ID, 'already-done-title'),
            'post_text' => carbon_get_post_meta($post->ID, 'already-done-text'),
            'post_image' => get_post(carbon_get_post_meta($post->ID, 'already-done-image'))->guid ?? '',
            'post_type' => $post->post_type
        ];
    }
}
