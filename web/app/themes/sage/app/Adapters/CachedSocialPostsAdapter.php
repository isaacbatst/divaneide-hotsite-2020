<?php

namespace App\Adapters;

use App\Adapters\CachedSocialPostsFacebookAdapter;

class CachedSocialPostsAdapter {
    public static function adaptFacebookPosts($posts){
        return array_map(function($post) {
            $facebookPostAdapter = new CachedSocialPostsFacebookAdapter($post);

            $adaptedPost = (object) $facebookPostAdapter->getFacebookPostAdapted();

            $adaptedPost->limited_caption = self::getPostLimitedCaption($adaptedPost->caption);
            $adaptedPost->title = self::getPostTitle($adaptedPost);

            return $adaptedPost;
        }, $posts);
    }

    public static function adaptInstagramPosts($posts) {
        return array_map(function($post) {
            $post->social_media = 'instagram';
            $post->caption = $post->caption ?? '';

            $post->limited_caption = self::getPostLimitedCaption($post->caption);
            $post->title = self::getPostTitle($post);

            return $post;
        }, $posts);
    }

    private static function getPostLimitedCaption($caption) {
        return strlen($caption) > 40 ? substr($caption, 0, 40) : $caption;
    }

    private static function getPostTitle($adaptedPost) {
        $socialMediaWithFirstLetterUppercased = self::uppercaseFirstLetter($adaptedPost->social_media);
        $titlePrefix = strlen($adaptedPost->caption) > 0 ? "{$adaptedPost->limited_caption} - " : "";

        return "{$titlePrefix}{$socialMediaWithFirstLetterUppercased} #{$adaptedPost->id}";
    }

    private static function uppercaseFirstLetter($string) {
        $firstLetter = substr($string, 0, 1);
        $rest = substr($string, 1, strlen($string));

        return strtoupper($firstLetter).$rest;
    }
}
