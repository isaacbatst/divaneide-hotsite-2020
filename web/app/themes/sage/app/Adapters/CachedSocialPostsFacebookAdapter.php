<?php

namespace App\Adapters;

class CachedSocialPostsFacebookAdapter {
    public function __construct($post)
    {
        $this->post = $post;
    }

    public function getFacebookPostAdapted() {
        return [
            'id' => $this->post->id,
            'timestamp' => $this->post->created_time,
            'permalink' => $this->post->permalink_url,
            'caption' => $this->post->message ?? '',
            'media_url' => $this->getFacebookPostMediaUrl(),
            'media_type' => $this->getFacebookPostMediaType(),
            'thumbnail_url' => $this->getFacebookPostThumbnail(),
            'social_media' => 'facebook'
        ];
    }

    private function getFacebookPostMediaUrl() {
        if($this->post->type ==="photo") {
            return $this->post->full_picture;
        }

        if($this->post->type === "video") {
            return $this->post->source ?? '';
        }

        return "";
    }

    private function getFacebookPostMediaType() {
        if($this->post->type === "photo") {
            return "IMAGE";
        }

        if($this->post->type === "video") {
            return "VIDEO";
        }

        return "STATUS";
    }

    private function getFacebookPostThumbnail() {
        if($this->post->type === 'video') {
            return $this->post->full_picture;
        }

        return "";
    }
}
