<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateInterno extends Controller
{
    public function __construct()
    {
        $this->categories = [];

        $terms = get_the_terms( get_the_ID() , 'page-category');

        if($terms) {
            foreach($terms as $category) {
                $this->categories[] = $category->slug;
            }
        }
    }

    public function relatedAlreadyDone(){
        $relatedPosts = new \WP_Query([
            'post_type' => 'already-done',
            'posts_per_page' => 4,
            'status' => 'published',
            'post__not_in' => array(get_the_ID()),
            'tax_query' => [
                [
                    'taxonomy' => 'page-category',
                    'field' => 'slug',
                    'terms' => $this->categories
                ]
            ],
            'orderby' => 'date',
            'order' => 'DESC'
        ]);

        return $relatedPosts;
    }

    public function relatedProposals(){
        $relatedPosts = new \WP_Query([
            'post_type' => 'proposal',
            'posts_per_page' => 4,
            'status' => 'published',
            'post__not_in' => array(get_the_ID()),
            'tax_query' => [
                [
                    'taxonomy' => 'page-category',
                    'field' => 'slug',
                    'terms' => $this->categories
                ]
            ],
            'orderby' => 'date',
            'order' => 'DESC'
        ]);

        return $relatedPosts;
    }
}
