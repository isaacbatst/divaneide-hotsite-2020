<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use App\Repositories\PagesRepository;
use App\Repositories\CachedSocialPostsRepository;

class PageSaibaMais extends Controller
{
    public function __construct()
    {
        $this->categories = [];

        $terms = get_the_terms(get_the_ID(), 'page-category');

        if ($terms) {
            foreach ($terms as $category) {
                $this->categories[] = $category->slug;
            }
        }
    }

    public function relatedPosts()
    {
        $query = $_GET['query'] ?? '';

        $pagesRepository = new PagesRepository();
        $socialPostsRepository = new CachedSocialPostsRepository();
        $external = $query == 'divafez' || $query == 'proposta' ? [] : $socialPostsRepository->searchSocialPosts($query);

        $limit = -1;

        $matchingPosts = [
            'internal' => $pagesRepository->searchOnInternalPosts($query, $limit),
            'external' => $external
        ];

        return $matchingPosts;
    }
}
