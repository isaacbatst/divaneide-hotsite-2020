<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use App\Providers\InstagramProvider;
use GuzzleHttp\Exception\ClientException;

class PageReceiveInstagramAuth extends Controller
{
    public function processAuth()
    {
        if ($this->receivedCodeFromInstagramOAuth()) {
            try {
                $accessToken = $this->exchangeCodeForAccessToken();
                return $this->saveAccessTokenAndRedirect($accessToken);
            } catch (ClientException $e) {
                //todo fazer algo com esse erro
                header('Location: ' . env('WP_HOME'));
            }
        }

        header('Location: ' . env('WP_HOME'));
    }

    private function receivedCodeFromInstagramOAuth()
    {
        return isset($_GET['code']) && $_GET['code'];
    }

    private function exchangeCodeForAccessToken(){
        $instagramProvider = new InstagramProvider();
        return $instagramProvider->getAccessToken($_GET['code']);
    }

    private function saveAccessTokenAndRedirect($accessToken) {
        carbon_set_theme_option(
            'social-medias-api-access-instagram-access-token',
            $accessToken
        );

        return header('Location: ' . env('WP_HOME') . '/wp/wp-admin');
    }
}
