<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleProposal extends Controller
{
    public function __construct()
    {
        $this->categories = [];

        $terms = get_the_terms( get_the_ID() , 'page-category');

        if($terms) {
            foreach($terms as $category) {
                $this->categories[] = $category->slug;
            }
        }
    }

    public function relatedPosts(){
        $relatedPosts = new \WP_Query([
            'post_type' => 'proposal',
            'posts_per_page' => -1,
            'status' => 'published',
            'post__not_in' => array(get_the_ID()),
            'tax_query' => [
                [
                    'taxonomy' => 'page-category',
                    'field' => 'slug',
                    'terms' => $this->categories
                ]
            ],
            'orderby' => 'date',
            'order' => 'DESC'
        ]);

        return $relatedPosts->get_posts();
    }
}
