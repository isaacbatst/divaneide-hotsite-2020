<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    public function whatsappLink() {
        $whatsappMessage = carbon_get_theme_option('social-medias-contact-whatsapp-message');
        $whatsappMessage = urlencode($whatsappMessage);
        $whatsappNumber = carbon_get_theme_option('social-medias-contact-whatsapp');

        return "https://wa.me/{$whatsappNumber}?text={$whatsappMessage}";
    }

    public function guidelines() {

        $result = new \WP_Query([
            'post_type' => 'page',
            'posts_per_page' => -1,
            'status' => 'published',
            'post__not_in' => [get_the_ID(), get_option('page_on_front')],
            'meta_query' => [
                [
                    'key' => '_wp_page_template',
                    'value' => 'views/template-interno.blade.php'
                ]
            ],
            'orderby' => 'date',
            'order' => 'DESC'
        ]);

        return $result->get_posts();
    }
}
