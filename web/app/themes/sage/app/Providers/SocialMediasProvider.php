<?php

namespace App\Providers;

use App\Providers\InstagramProvider;
use App\Providers\FacebookProvider;

class SocialMediasProvider
{
    public function fetchSocialPosts()
    {
        $tokens = $this->getAccessTokens();

        $instagramPosts = $this->getInstagramPosts($tokens['instagram']);
        $facebookPosts = $this->getFacebookPosts($tokens['facebook']);

        return [
            'instagram' => $instagramPosts,
            'facebook' => $facebookPosts
        ];
    }

    private function getAccessTokens()
    {
        return [
            'instagram' => carbon_get_theme_option('social-medias-api-access-instagram-access-token'),
            'facebook' => carbon_get_theme_option('social-medias-api-access-facebook-access-token')
        ];
    }


    private function getInstagramPosts($accessToken)
    {
        $instagramProvider = new InstagramProvider($accessToken);
        return $instagramProvider->getMedia();
    }

    private function getFacebookPosts($accessToken) {
        $facebookProvider = new FacebookProvider($accessToken);
        return $facebookProvider->getMedia();
    }
}
