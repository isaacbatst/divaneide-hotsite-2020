<?php

namespace App\Providers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class InstagramProvider
{
    public function __construct($accessToken = "")
    {
        $this->graphClient = $this->createGraphHttpClient();
        $this->accessToken = $accessToken;
    }

    public function isAccessTokenValid()
    {
        $accessToken = carbon_get_theme_option('social-medias-api-access-instagram-access-token');

        try {
            $this->graphClient->get('/me/media', [
                'query' => [
                    'access_token' => $accessToken
                ]
            ]);

            return [
                'isValid' => true
            ];
        } catch (ClientException $e) {
            return [
                'isValid' => false,
                'error' => $e
            ];
        } catch (RequestException $e) {
            return [
                'isValid' => false,
                'error' => $e
            ];
        }
    }

    private function createGraphHttpClient()
    {
        return new Client([
            'base_uri' => 'https://graph.instagram.com',
            'timeout'  => 4.0,
        ]);
    }

    private function createApiHttpClient()
    {
        return new Client([
            'base_uri' => 'https://api.instagram.com',
            'timeout'  => 4.0,
        ]);
    }

    public function oauthInstagram($state = "")
    {
        $authQuery = http_build_query([
            'client_id' => env('INSTAGRAM_APP_ID'),
            'redirect_uri' => env('WP_HOME').'/receive-instagram-auth',
            'scope' => 'user_profile,user_media',
            'response_type' => 'code',
            'state' => $state
        ]);

        $auth_request_url = "https://api.instagram.com/oauth/authorize?{$authQuery}";

        header("Location: " . $auth_request_url);
    }

    public function getAccessToken($code)
    {
        $client = $this->createApiHttpClient();

        $response = $client->post('/oauth/access_token', [
            'form_params' => [
                'client_id' => env('INSTAGRAM_APP_ID'),
                'client_secret' => env('INSTAGRAM_APP_SECRET'),
                'code' => $code,
                'grant_type' => 'authorization_code',
                'redirect_uri' => env('WP_HOME').'/receive-instagram-auth'
            ]
        ]);

        return json_decode($response->getBody()->getContents())->access_token;
    }

    public function getMedia()
    {
        try {
            $response = $this->graphClient->get('/me/media', [
                'query' => [
                    'fields' => 'id,caption,media_url,permalink,timestamp, media_type, thumbnail_url',
                    'access_token' => $this->accessToken,
                ]
            ]);

            return [
                'posts' => json_decode($response->getBody()->getContents())->data,
            ];
        } catch (ClientException $e) {
            return [
                'posts' => [],
                'unauthenticated' => true
            ];
        } catch (RequestException $e) {
            return [
                'posts' => [],
                'unauthenticated' => true
            ];
        }
    }
}
