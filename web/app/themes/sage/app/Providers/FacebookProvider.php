<?php

namespace App\Providers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class FacebookProvider
{
    public function __construct($accessToken = "")
    {
        $this->graphClient = $this->createGraphHttpClient();
        $this->accessToken = $accessToken;

        defined('FB_INVALID_ACCESS_TOKEN_ERROR_CODE') || define('FB_INVALID_ACCESS_TOKEN_ERROR_CODE', 190);
    }

    private function createGraphHttpClient()
    {
        return new Client([
            'base_uri' => 'https://graph.facebook.com/v8.0',
            'timeout'  => 20,
        ]);
    }

    public function getMedia()
    {
        try {
            $response = $this->graphClient->get('/me/posts', [
                'query' => [
                    'access_token' => $this->accessToken,
                    'fields' => 'type, name, created_time, message,  permalink_url, source, full_picture, properties',
                ]
            ]);

            return [
                'posts' => json_decode($response->getBody()->getContents())->data
            ];
        } catch (ClientException $e) {
            $this->handleErrors($e);

            return [
                'posts' => [],
                'unauthenticated' => true,
                'error' => $e->getCode(),
                'message' => json_decode($e->getResponse()->getBody()->getContents())
            ];
        } catch (RequestException $e) {
            return [
                'posts' => [],
                'unauthenticated' => true,
                'error' => $e->getCode(),
                'message' => json_decode($e->getResponse()->getBody()->getContents())
            ];
        }
    }

    private function handleErrors(ClientException $e)
    {
        $response = $e->getResponse()->getBody()->getContents();
        $response = json_decode($response);

        $facebookErrorCode = $response->error->code;

        if ($facebookErrorCode === FB_INVALID_ACCESS_TOKEN_ERROR_CODE) {
        }
    }
}
