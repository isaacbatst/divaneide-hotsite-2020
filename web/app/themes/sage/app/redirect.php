<?php

namespace App;

$url = "http";

if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
    $url = "https";
}

$url .= "://";

$url .= $_SERVER['HTTP_HOST'];

$request = $_SERVER['REQUEST_URI'];

if ($url == env('WP_HOME') && ($request == '/propostas' || $request == '/propostas/')) {
    header("Location: $url/saiba-mais?query=proposta", true, 301);
    exit;
}

if ($url == env('WP_HOME') && ($request == '/divafez' || $request == '/divafez/')) {
    header("Location: $url/saiba-mais?query=divafez", true, 301);
    exit;
}
