<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('carbon_fields_register_fields', 'crb_attach_layout_elements_theme_options');

function crb_attach_layout_elements_theme_options()
{
    Container::make_theme_options(__('Elementos do layout'))
        ->set_icon('dashicons-admin-appearance')
        ->add_tab('Geral', array(
            Field::make_association('layout-elements-active-thematic-week', 'Semana Temática ativa')
                ->set_types([
                    [
                        'type' => 'post',
                        'post_type' => 'thematic-week'
                    ]
                ])
                ->set_max(1)
                ->set_help_text('Selecione uma Semana Temática que estará ativa e será utilizada pelo layout do site'),
            Field::make_text('layout-elements-text-stand-by', 'Text de fora do ar'),
            Field::make_checkbox('layout-elements-is-stand-by', 'Fora do ar')
        ))
        ->add_tab('Internas', array(
            Field::make_text('layout-elements-proposals-title', 'Título das internas de Propostas')
                ->set_default_value('Propostas'),
            Field::make_text('layout-elements-already-done-title', 'Título das internas de DivaFez')
                ->set_default_value('#DivaFez')
        ))
        ->add_tab('Menu', array(
            Field::make_complex('layout-elements-menu-custom-links', 'Links personalizados do menu')
                ->set_min('3')
                ->set_max('5')
                ->add_fields('layout-elements-menu-custom-link', 'Item personalizado', array(
                    Field::make_text('layout-elements-menu-custom-link-label', 'Texto do item'),
                    Field::make_text('layout-elements-menu-custom-link-url', 'URL do item'),
                ))
        ))
        ->add_tab('Downloads', array(
            Field::make_complex('layout-elements-downloads', 'Arquivos para Download')
                ->add_fields('layout-elements-downloadable', 'Baixável', array(
                    Field::make_file('layout-elements-downloadable-file', 'Arquivo'),
                    Field::make_text('layout-elements-downloadable-name', 'Nome')
                ))
        ))
        ->add_tab('Rodapé', array(
            Field::make_text('layout-elements-footer-contact-title', 'Título do Contato')
                ->set_default_value('Deixe seu Whatsapp!')
        ));
}
