<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('carbon_fields_register_fields', 'crb_attach_social_media_theme_options');

function crb_attach_social_media_theme_options()
{
    Container::make_theme_options(__('Redes Sociais'))
        ->set_icon('dashicons-share')
        ->add_tab('Contato', array(
            Field::make_checkbox('social-medias-contact-whatsapp-show', 'Mostrar botão do Whatsapp?')
                ->set_default_value(true),
            Field::make('text', 'social-medias-contact-whatsapp', 'Whatsapp')
                ->set_help_text('Número do WhatsApp - Formato: 5584999999999')
                ->set_conditional_logic([
                    [
                        'field' => 'social-medias-contact-whatsapp-show',
                        'value' => true,
                    ]
                ]),
            Field::make('text', 'social-medias-contact-whatsapp-message', 'Mensagem padrão do Whatsapp')
                ->set_help_text('Será a mensagem padrão para nos enviar quando o usuário clicar no botão do whatsapp')
                ->set_conditional_logic([
                    [
                        'field' => 'social-medias-contact-whatsapp-show',
                        'value' => true,
                    ]
                ]),
            Field::make_checkbox('social-medias-contact-twitter-show', 'Mostrar botão do Twitter?')
                ->set_default_value(true),
            Field::make('text', 'social-medias-contact-twitter', 'Twitter')
                ->set_help_text('Link para o perfil')
                ->set_conditional_logic([
                    [
                        'field' => 'social-medias-contact-twitter-show',
                        'value' => true,
                    ]
                ]),
            Field::make_checkbox('social-medias-contact-facebook-show', 'Mostrar botão do Facebook?')
                ->set_default_value(true),
            Field::make('text', 'social-medias-contact-facebook', 'Facebook')
                ->set_help_text('Link para o perfil')
                ->set_conditional_logic([
                    [
                        'field' => 'social-medias-contact-facebook-show',
                        'value' => true,
                    ]
                ]),
            Field::make_checkbox('social-medias-contact-instagram-show', 'Mostrar botão do Instagram?')
                ->set_default_value(true),
            Field::make('text', 'social-medias-contact-instagram', 'Instagram')
                ->set_help_text('Link para o perfil')
                ->set_conditional_logic([
                    [
                        'field' => 'social-medias-contact-instagram-show',
                        'value' => true,
                    ]
                ]),
            Field::make_checkbox('social-medias-contact-chip-in-show', 'Mostrar botão da vaquinha?')
                ->set_default_value(true),
            Field::make('text', 'social-medias-contact-chip-in', 'Vaquinha')
                ->set_help_text('Link para a vaquinha')
                ->set_conditional_logic([
                    [
                        'field' => 'social-medias-contact-chip-in-show',
                        'value' => true,
                    ]
                ]),
            Field::make('text', 'social-medias-contact-chip-in-text', 'Texto para o botão da vaquinha!')
                ->set_default_value('Ajude a campanha!')
                ->set_conditional_logic([
                    [
                        'field' => 'social-medias-contact-chip-in-show',
                        'value' => true,
                    ]
                ]),
        ))
        ->add_tab('Acessos API', array(
            Field::make('text', 'social-medias-api-access-instagram-access-token', 'Instagram - Access Token')
                ->set_attributes([
                    'readOnly' => true
                ]),
            Field::make('text', 'social-medias-api-access-facebook-access-token', 'Facebook - Access Token')
                ->set_attributes([
                    'readOnly' => true
                ])
                ->set_visible_in_rest_api(true)
        ));
}
