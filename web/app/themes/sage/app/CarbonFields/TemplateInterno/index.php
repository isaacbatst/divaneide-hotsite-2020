<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('carbon_fields_register_fields', 'attach_internal_page_fields', 11);

function attach_internal_page_fields()
{
    Container::make_post_meta('Conteúdo da página')
        ->set_context('carbon_fields_after_title')
        ->where('post_template', '=', 'views/template-interno.blade.php')

        ->add_tab(__('Geral'), array(
            Field::make('text', 'template-interno-title', 'Título da Página')->set_visible_in_rest_api(),
            Field::make('rich_text', 'template-interno-text', __('Texto corrido')),
            Field::make_image('template-interno-image', __('Imagem'))
        ));
}
