<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;
use App\Providers\SocialMediasProvider;
use App\Repositories\CachedSocialPostsRepository;

add_action('carbon_fields_register_fields', 'attach_front_page_fields', 11);

function attach_front_page_fields()
{
    //O instagram limita a qtd de requisições, então só estou requisitando na edição da Home
    if (isEditingPageOnFront()) {
        fetchAndSaveSocialPosts();
    }

    Container::make_post_meta('Página Inicial')
        ->set_context('carbon_fields_after_title')
        ->where('post_id', '=', get_option('page_on_front'))

        ->add_tab(__('Banner'), array(
            Field::make('text', 'front-page-banner-section-id', 'ID da Seção')
                ->set_help_text('Adicione apenas texto com letras sempre minúsculas, e com as palavras separadas por "-", e não por espaço.'),
            Field::make('text', 'front-page-banner-title', __('Título')),
            Field::make('text', 'front-page-banner-description', __('Descrição')),
            Field::make_color('front-page-banner-background-color', __('Cor de Fundo'))
                ->set_palette( array( '#f04', '#2cb6e7', '#5ad38e', '#ffca00', '#fa6666', '#8a00b8' ) ),
            Field::make('image', 'front-page-banner-image-desktop', __('Imagem Desktop'))
                ->set_help_text('Proporção: 1440 x 1000'),
            Field::make('image', 'front-page-banner-image-mobile', __('Imagem Mobile'))
                ->set_help_text('Proporção: 180 x 180'),
        ))

        ->add_tab(__('Sugestões - Cata lead'), array(
            Field::make('text', 'front-page-cata-lead-section-id', 'ID da Seção')
                ->set_help_text('Adicione apenas texto com letras sempre minúsculas, e com as palavras separadas por "-", e não por espaço.'),
            Field::make('text', 'front-page-cata-lead-title', __('Título')),
            Field::make('text', 'front-page-cata-lead-subtitle', __('Subtítulo')),
            Field::make_text('front-page-cata-lead-legal-text', 'Texto Legal')
                ->set_default_value('*Aceito que minha proposta e meu nome sejam exibidos publicamente nesse site. Aceito receber novidades pelo Whatsapp.')
        ))

        ->add_tab(__('Hard news - Feed das redes'), array(
            Field::make('text', 'front-page-hard-news-section-id', 'ID da Seção')
                ->set_help_text('Adicione apenas texto com letras sempre minúsculas, e com as palavras separadas por "-", e não por espaço.'),
            Field::make('text', 'front-page-hard-news-title', __('Título')),
            Field::make_color('front-page-hard-news-background-color', __('Cor de Fundo'))
                ->set_palette( array( '#f04', '#2cb6e7', '#5ad38e', '#ffca00', '#fa6666', '#8a00b8' ) ),
            Field::make_color('front-page-hard-news-text-color', __('Cor do Título'))
                ->set_palette( array( '#f04', '#2cb6e7', '#5ad38e', '#ffca00', '#fa6666', '#8a00b8' ) ),
            Field::make('image', 'front-page-hard-news-background-desktop', __('Imagem Desktop'))
                ->set_help_text('Proporção: 1440 x 1000'),
            Field::make_association('front-page-hard-news-posts', 'Posts')
                ->set_types([
                    [
                        'type' => 'post',
                        'post_type' => 'cached-social-post'
                    ]
                ])
                ->set_help_text('Selecione os posts que serão visíveis')
                ->set_classes('social-posts-association')
            ),
        )

        ->add_tab(__('Conteudo em destaque'), array(
            Field::make('text', 'front-page-spotlight-content-section-id', 'ID da Seção')
                ->set_help_text('Adicione apenas texto com letras sempre minúsculas, e com as palavras separadas por "-", e não por espaço.'),
            Field::make('text', 'front-page-spotlight-content-title', __('Título')),
            Field::make('rich_text', 'front-page-spotlight-content-text', __('Texto')),
            Field::make_color('front-page-spotlight-content-background-color', __('Cor de Fundo'))
                ->set_palette( array( '#f04', '#2cb6e7', '#5ad38e', '#ffca00', '#fa6666', '#8a00b8' ) ),
            Field::make_color('front-page-spotlight-content-text-color', __('Cor do Título'))
                ->set_palette( array( '#f04', '#2cb6e7', '#5ad38e', '#ffca00', '#fa6666', '#8a00b8' ) ),
            Field::make('image', 'front-page-spotlight-content-background-desktop', __('Imagem de fundo Desktop'))
                ->set_help_text('Proporção: 1440 x 1000'),
            Field::make_select('front-page-spotlight-content-content-type', 'Tipo de conteúdo' )
                ->add_options( array(
                    'video' => 'Vídeo',
                    'img' => 'Imagem',
            )),
            Field::make_text('front-page-spotlight-content-video-url', __('URL do vídeo'))
                ->set_conditional_logic(array(
                    'relation' => 'AND',
                    array(
                        'field' => 'front-page-spotlight-content-content-type',
                        'value' => 'video',
                        'compare' => '=',
                    )
            ))
            ->set_help_text( 'Id da url depois de "https://www.youtube.com/watch?v=", ex: mzjZ0YMei4k' ),
            Field::make_image('front-page-spotlight-content-img', __('Imagem'))
                ->set_conditional_logic(array(
                    'relation' => 'AND',
                    array(
                        'field' => 'front-page-spotlight-content-content-type',
                        'value' => 'img',
                        'compare' => '=',
                    )
                )),
            Field::make_checkbox('front-page-spotlight-content-button-show', 'Ativar botão'),
            Field::make_text('front-page-spotlight-content-button-text', __('Texto do botão'))
            ->set_conditional_logic(array(
                array(
                    'field' => 'front-page-spotlight-content-button-show',
                    'value' => true,
                )
            )),
            Field::make( 'file', 'front-page-spotlight-content-button-file', __('Arquivo do botão') )
            ->set_conditional_logic(array(
                array(
                    'field' => 'front-page-spotlight-content-button-show',
                    'value' => true,
                )
            ))
        ))

        ->add_tab(__('Depoimentos'), array(
            Field::make('text', 'front-page-depoimentos-section-id', 'ID da Seção')
                ->set_help_text('Adicione apenas texto com letras sempre minúsculas, e com as palavras separadas por "-", e não por espaço.'),
            Field::make('text', 'front-page-depoimentos-title', __('Título')),
            Field::make_color('front-page-depoimentos-background-color', __('Cor de Fundo'))
                ->set_palette( array( '#f04', '#2cb6e7', '#5ad38e', '#ffca00', '#fa6666', '#8a00b8' ) ),
            Field::make_color('front-page-depoimentos-text-color', __('Cor do Título'))
                ->set_palette( array( '#f04', '#2cb6e7', '#5ad38e', '#ffca00', '#fa6666', '#8a00b8' ) ),
            Field::make('image', 'front-page-depoimentos-background-desktop', __('Imagem Desktop'))
                ->set_help_text('Proporção: 1440 x 1000'),
            Field::make_complex('front-page-depoimentos-depoimentos', 'Depoimentos')
                ->add_fields('depoimento', 'Depoimento', array(
                    Field::make_image('image', 'Foto'),
                    Field::make_text('text', 'Texto'),
                    )
                )
                ->set_max(3)
            ),
        )

        ->add_tab(__('Pautas'), array(
            Field::make('text', 'front-page-pautas-section-id', 'ID da Seção')
                ->set_help_text('Adicione apenas texto com letras sempre minúsculas, e com as palavras separadas por "-", e não por espaço.'),
            Field::make('text', 'front-page-pautas-title', __('Título')),
            Field::make_color('front-page-pautas-background-color', __('Cor de Fundo'))
                ->set_palette( array( '#f04', '#2cb6e7', '#5ad38e', '#ffca00', '#fa6666', '#8a00b8' ) ),
            Field::make_color('front-page-pautas-text-color', __('Cor do Título'))
                ->set_palette( array( '#f04', '#2cb6e7', '#5ad38e', '#ffca00', '#fa6666', '#8a00b8' ) ),
            Field::make('image', 'front-page-pautas-background-desktop', __('Imagem de fundo Desktop'))
                ->set_help_text('Proporção: 1440 x 1000'),
            Field::make_association('front-page-pautas-pautas','Pautas')
                ->set_types([
                    [
                        'type' => 'post',
                        'post_type' => 'page'
                    ]
                ])
        ));
    //todo seção coringa
}

function isEditingPageOnFront()
{
    return isset($_GET['post']) && isset($_GET['action']) && $_GET['action'] === 'edit' && $_GET['post'] === get_option('page_on_front');
}

function fetchAndSaveSocialPosts()
{
    $socialMediasProvider = new SocialMediasProvider();
    $socialPosts = $socialMediasProvider->fetchSocialPosts();

    $cachedSocialPostsRepository = new CachedSocialPostsRepository();
    $cachedSocialPostsRepository->saveSocialPosts($socialPosts);
}

add_filter('carbon_fields_association_field_options', 'editSocialPostsAssociation', 11, 5);

function editSocialPostsAssociation($posts)
{

    return array_map(function ($post) {
        $media_url = getSocialPostThumb($post);

        $caption = carbon_get_post_meta($post['id'], 'cached-social-post-caption');

        return array_merge($post, [
            'thumbnail' => $media_url,
            'title' => strlen($caption) === 0 ? get_the_title($post['id']) : $caption,
        ]);
    }, $posts);
}

function getSocialPostThumb($post)
{
    $media_type = carbon_get_post_meta($post['id'], 'cached-social-post-media-type');

    if ($media_type === 'VIDEO') {
        return carbon_get_post_meta($post['id'], 'cached-social-post-thumbnail-url');
    }

    return carbon_get_post_meta($post['id'], 'cached-social-post-media-url');
}

add_filter('carbon_fields_association_field_options_front-page-hard-news-posts_post_cached-social-post', 'filter_facebook_posts', 11, 1);

function filter_facebook_posts($query_options)
{
    return array_merge($query_options, [
        'meta_query' => [
            [
                'key' => 'cached-social-post-media-type',
                'compare' => '!=',
                'value' => 'STATUS'
            ],
        ]
    ]);
}

add_filter('carbon_fields_association_field_options_front-page-pautas-pautas_post_page', 'filter_pautas', 11, 1);

function filter_pautas($query_options)
{
    return array_merge($query_options, [
        'meta_key' => '_wp_page_template',
        'meta_value' => 'views/template-interno.blade.php'
    ]);
}