<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action('carbon_fields_register_fields', 'attach_download_page_fields', 11);

function attach_download_page_fields()
{
    Container::make_post_meta('Conteúdo da página')
        ->set_context('carbon_fields_after_title')
        ->where('post_template', '=', 'views/template-downloads.blade.php')

        ->add_tab(__('Geral'), array(
            Field::make('text', 'template-downloads-title', 'Título da Página'),
        ));
}
