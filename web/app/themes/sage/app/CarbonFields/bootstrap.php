<?php

require(dirname(__FILE__) . '/FrontPage/index.php');
require(dirname(__FILE__) . '/TemplateInterno/index.php');
require(dirname(__FILE__) . '/TemplateDownloads/index.php');
require(dirname(__FILE__) . '/ThemeOptions/index.php');

add_action('after_setup_theme', 'crb_load');

function crb_load()
{
    require_once(ABSPATH . '../../vendor/autoload.php');
    \Carbon_Fields\Carbon_Fields::boot();
}
