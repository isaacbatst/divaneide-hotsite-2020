<?php

namespace App\Repositories;
use App\Adapters\CachedSocialPostsAdapter;
use WP_Post;

class CachedSocialPostsRepository
{
    public function saveSocialPosts($socialPosts)
    {
        $instagramPosts = CachedSocialPostsAdapter::adaptInstagramPosts($socialPosts['instagram']['posts']);
        $facebookPosts = CachedSocialPostsAdapter::adaptFacebookPosts($socialPosts['facebook']['posts']);

        $posts = [
            ...$instagramPosts,
            ...$facebookPosts
        ];

        $this->savePosts($posts);
    }

    public function searchSocialPosts($query) {
        $posts = $this->getPosts($query);
        $postsWithFields = $this->getPostsFields($posts);
        return $postsWithFields;
    }

    private function getPosts($query) {
        $result = new \WP_Query([
            'post_type' => 'cached-social-post',
            'posts_per_page' => 6,
            'status' => 'published',
            'post__not_in' => array(get_the_ID()),
            'meta_query' => [
                'relation' => 'OR',
                [
                    'key' => 'cached-social-post-caption',
                    'compare' => 'LIKE',
                    'value' => $query
                ],
            ],
            'orderby' => 'date',
            'order' => 'DESC'
        ]);

        return $result->get_posts();
    }

    private function getPostsFields($posts) {
        return array_map(function(WP_Post $post) {
            return [
                'ID' => $post->ID,
                'post_date' => $post->post_date,
                'post_social_media' => carbon_get_post_meta($post->ID, 'cached-social-post-social-media'),
                'post_caption' => carbon_get_post_meta($post->ID, 'cached-social-post-caption'),
                'post_media_url' => carbon_get_post_meta($post->ID, 'cached-social-post-media-url'),
                'post_media_type' => carbon_get_post_meta($post->ID, 'cached-social-post-media-type'),
                'post_permalink' => carbon_get_post_meta($post->ID, 'cached-social-post-permalink'),
                'post_thumb_url' => carbon_get_post_meta($post->ID, 'cached-social-post-thumbnail-url')
            ];
        }, $posts);
    }

    private function savePosts($posts)
    {
        array_walk($posts, function ($post) {
            $duplicadedPosts = $this->findDuplicatedPosts($post->id);

            $this->createPost($post, $duplicadedPosts);
        });
    }

    private function findDuplicatedPosts($id)
    {
        $query = new \WP_Query([
            'post_type' => 'cached-social-post',
            'meta_query' => [
                [
                    'key' => 'cached-social-post-external-id',
                    'value' => $id
                ]
            ]
        ]);

        return $query->get_posts();
    }

    private function createPost($post, $duplicadedPosts = [])
    {
        $post_id = $this->insertWordpressPost($post, $duplicadedPosts);

        if ($post_id) {
            $this->setCarbonFields($post_id, $post);
        }
    }

    private function insertWordpressPost($post, $duplicadedPosts)
    {
        $postToSave = [
            'post_title' => "{$post->title}",
            'post_status' => 'publish',
            'post_type' => 'cached-social-post',
        ];

        if(count($duplicadedPosts) === 0) {
            $post_id = wp_insert_post($postToSave);
            return $post_id;
        }

        return $duplicadedPosts[0]->ID;
    }

    private function setCarbonFields($post_id, $post)
    {
        carbon_set_post_meta($post_id,  'cached-social-post-external-id', $post->id);
        carbon_set_post_meta($post_id,  'cached-social-post-caption', $post->caption);
        carbon_set_post_meta($post_id,  'cached-social-post-media-url', $post->media_url);
        carbon_set_post_meta($post_id,  'cached-social-post-media-type', $post->media_type);
        carbon_set_post_meta($post_id,  'cached-social-post-thumbnail-url', $post->thumbnail_url ?? '');
        carbon_set_post_meta($post_id,  'cached-social-post-permalink', $post->permalink);
        carbon_set_post_meta($post_id,  'cached-social-post-timestamp', $post->timestamp);
        carbon_set_post_meta($post_id, 'cached-social-post-social-media', $post->social_media);
    }

}
