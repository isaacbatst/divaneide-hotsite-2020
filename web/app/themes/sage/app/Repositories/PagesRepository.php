<?php

namespace App\Repositories;
use WP_Post;
use App\Adapters\InternalPostsAdapter;

class PagesRepository
{
    public function searchOnInternalPosts($query, $limit = false)
    {
        $categoriesMatchingPosts = $this->searchByCategories($query, $limit);
        $contentMatchingPosts = $this->searchByContent($query, $categoriesMatchingPosts, $limit);

        $posts = [
            ...$categoriesMatchingPosts,
            ...$contentMatchingPosts
        ];

        return $this->getInternalPostsFields($posts);
    }

    private function getInternalPostsFields($posts)
    {
        return array_map(function (WP_Post $post) {
            if($post->post_type === 'page' && get_page_template_slug($post->ID) === 'views/template-interno.blade.php') {
                return InternalPostsAdapter::adaptTemplateInterno($post);
            }

            if($post->post_type === 'proposal') {
                return InternalPostsAdapter::adaptProposal($post);
            }

            if($post->post_type === 'already-done') {
                return InternalPostsAdapter::adaptAlreadyDone($post);
            }

            return [];
        }, $posts);
    }



    private function searchByCategories($query, $limit = false)
    {
        $result = new \WP_Query([
            'post_type' => ['page', 'proposal', 'already-done'],
            'posts_per_page' => $limit ?? 6,
            'status' => 'published',
            'post__not_in' => array(get_the_ID()),
            'tax_query' => [
                [
                    'taxonomy' => 'page-category',
                    'field' => 'slug',
                    'terms' => $query
                ]
            ],
            'orderby' => 'date',
            'order' => 'DESC'
        ]);

        return $result->get_posts();
    }

    private function searchByContent($query, $posts, $limit = false)
    {
        $postsIDs = $this->getPostsIDs($posts);

        $result = new \WP_Query([
            'post_type' => ['page', 'proposal', 'already-done'],
            'posts_per_page' => $limit ?? 4,
            'status' => 'published',
            'post__not_in' => [get_the_ID(), get_option('page_on_front'), ...$postsIDs],
            'meta_query' => [
                'relation' => 'OR',
                [
                    'key' => 'template-interno-title',
                    'compare' => 'LIKE',
                    'value' => $query
                ],
                [
                    'key' => 'template-interno-text',
                    'compare' => 'LIKE',
                    'value' => $query
                ],
                [
                    'key' => 'proposal-text',
                    'compare' => 'LIKE',
                    'value' => $query
                ],
                [
                    'key' => 'proposal-text',
                    'compare' => 'LIKE',
                    'value' => $query
                ],
                [
                    'key' => 'proposal-search-matching',
                    'compare' => 'LIKE',
                    'value' => $query
                ],
                [
                    'key' => 'already-done-text',
                    'compare' => 'LIKE',
                    'value' => $query
                ],
                [
                    'key' => 'already-done-text',
                    'compare' => 'LIKE',
                    'value' => $query
                ],
                [
                    'key' => 'already-done-search-matching',
                    'compare' => 'LIKE',
                    'value' => $query
                ],
            ],
            'orderby' => 'date',
            'order' => 'DESC'
        ]);

        return $result->get_posts();
    }

    private function getPostsIDs($posts)
    {
        return array_map(function (WP_Post $post) {
            return $post->ID;
        }, $posts);
    }
}
