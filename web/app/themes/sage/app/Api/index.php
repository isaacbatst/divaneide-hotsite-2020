<?php

namespace API;

use App\Providers\InstagramProvider;

require(dirname(__FILE__) . '/../Providers/index.php');
require(dirname(__FILE__) . '/searchPosts.php');

function areSocialMediasAuthenticated()
{
    header('Content-Type: application/json');

    $instagramProvider = new InstagramProvider();

    $tokenValidation = $instagramProvider->isAccessTokenValid();

    echo json_encode([
        'instagram' => $tokenValidation['isValid'],
        'facebook' => false
    ]);

    die();
}
add_action('wp_ajax_are_social_medias_authenticated', __NAMESPACE__ . '\\areSocialMediasAuthenticated');
add_action('wp_ajax_nopriv_are_social_medias_authenticated', __NAMESPACE__ . '\\areSocialMediasAuthenticated');

function authorizeInstagram()
{
    header('Content-Type: application/json');

    $instagramProvider = new InstagramProvider();

    $instagramProvider->oauthInstagram(['location' => $_GET['location']]);
}
add_action('wp_ajax_authorize_instagram', __NAMESPACE__ . '\\authorizeInstagram');
add_action('wp_ajax_nopriv_authorize_instagram', __NAMESPACE__ . '\\authorizeInstagram');

function saveFacebookAccessToken()
{
    header('Content-Type: application/json');

    if($_SERVER["REQUEST_METHOD"] !== "POST") {
        http_response_code(405);

        die(json_encode([
            'error' => 'invalid_request'
        ]));
    }

    $body = file_get_contents('php://input');
    $body = json_decode($body);

    if(!isset($body->accessToken)){
        http_response_code(400);

        die(json_encode([
            'error' => 'invalid_request'
        ]));
    }

    carbon_set_theme_option('social-medias-api-access-facebook-access-token', $body->accessToken);
}
add_action('wp_ajax_save_facebook_access_token', __NAMESPACE__ . '\\saveFacebookAccessToken');
add_action('wp_ajax_nopriv_save_facebook_access_token', __NAMESPACE__ . '\\saveFacebookAccessToken');

function deleteFacebookAccessToken()
{
    header('Content-Type: application/json');

    if($_SERVER["REQUEST_METHOD"] !== "DELETE") {
        http_response_code(405);

        die(json_encode([
            'error' => 'invalid_request'
        ]));
    }

    carbon_set_theme_option('social-medias-api-access-facebook-access-token', '');
    http_response_code(204);
}
add_action('wp_ajax_delete_facebook_access_token', __NAMESPACE__ . '\\deleteFacebookAccessToken');
add_action('wp_ajax_nopriv_delete_facebook_access_token', __NAMESPACE__ . '\\deleteFacebookAccessToken');

function deleteInstagramAccessToken()
{
    if($_SERVER["REQUEST_METHOD"] !== "DELETE") {
        http_response_code(405);

        die(json_encode([
            'error' => 'invalid_request'
        ]));
    }

    carbon_set_theme_option('social-medias-api-access-instagram-access-token', '');
    http_response_code(204);
}
add_action('wp_ajax_delete_instagram_access_token', __NAMESPACE__ . '\\deleteInstagramAccessToken');
add_action('wp_ajax_nopriv_delete_instagram_access_token', __NAMESPACE__ . '\\deleteInstagramAccessToken');
