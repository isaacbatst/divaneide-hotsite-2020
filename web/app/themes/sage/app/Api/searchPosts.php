<?php

namespace API;

use App\Repositories\CachedSocialPostsRepository;
use App\Repositories\PagesRepository;

function searchPosts()
{
    if ($_SERVER["REQUEST_METHOD"] !== "GET") {
        http_response_code(405);

        echo json_encode([
            'error' => 'invalid_request'
        ]);

        die();
    }

    $query = $_GET['query'];

    $pagesRepository = new PagesRepository();
    $socialPostsRepository = new CachedSocialPostsRepository();

    $matchingPosts = [
        'internal' => $pagesRepository->searchOnInternalPosts($query),
        'external' => $socialPostsRepository->searchSocialPosts($query)
    ];

    echo json_encode([
        'query' => $query,
        'matchingPosts' => $matchingPosts
    ]);

    die();
}
add_action('wp_ajax_search_posts', __NAMESPACE__ . '\\searchPosts');
add_action('wp_ajax_nopriv_search_posts', __NAMESPACE__ . '\\searchPosts');




function searchOnExernalPosts($query)
{
    //pesquisar no facebook
    //pesquisar no instagram

    return [];
}
