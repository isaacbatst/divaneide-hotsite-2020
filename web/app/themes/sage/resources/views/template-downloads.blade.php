{{--
  Template Name: Template de Downloads
--}}

@extends('layouts.app')

@section('content')
  @include('partials.sections.template-downloads.main-content')
  @include('partials.sections.collaborative-platform')
@endsection
