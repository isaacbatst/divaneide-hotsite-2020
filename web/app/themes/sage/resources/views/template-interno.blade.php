{{--
  Template Name: Template Interno
--}}

@extends('layouts.app')

@section('content')
  @include('partials.sections.template-interno.main-content')
  @include('partials.sections.collaborative-platform')
@endsection
