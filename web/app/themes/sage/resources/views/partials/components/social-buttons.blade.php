@if(carbon_get_theme_option('social-medias-contact-whatsapp-show'))
<a target="_blank" class="whatsapp" href="{{ $whatsapp_link }}">
  <i class="fab fa-whatsapp"></i>
</a>
@endif

@if(carbon_get_theme_option('social-medias-contact-facebook-show'))
  <a target="_blank" class="facebook" href="{{ carbon_get_theme_option('social-medias-contact-facebook') }}">
    <i class="fab fa-facebook-f"></i>
  </a>
@endif

@if(carbon_get_theme_option('social-medias-contact-instagram-show'))
  <a target="_blank" class="instagram" href="{{ carbon_get_theme_option('social-medias-contact-instagram') }}">
    <i class="fab fa-instagram"></i>
  </a>
@endif

@if(carbon_get_theme_option('social-medias-contact-twitter-show'))
  <a target="_blank" class="twitter" href="{{ carbon_get_theme_option('social-medias-contact-twitter') }}">
    <i class="fab fa-twitter"></i>
  </a>
@endif

@if(carbon_get_theme_option('social-medias-contact-chip-in-show'))
  <a target="_blank" class="chip-in" href="{{ carbon_get_theme_option('social-medias-contact-chip-in') }}">
    <span class="label">{{ carbon_get_theme_option('social-medias-contact-chip-in-text') }}</span>
    <i class="fas fa-donate"></i>
  </a>
@endif
