<header>
  <div class="container">
   <div class="nav-wrapper">
    <nav class="navbar navbar-expand-lg navbar-dark">
      <div class="nav-header-wrapper">
        <a class="brand" href="{{ home_url('/') }}">
          <picture>
            <source srcset="@asset('images/logo2-pink.png')" media="(max-width: 998px)">
            <img class="img-fluid" src="@asset('images/logo-vertical-white.png')" alt="">
          </picture>
        </a>
        <button class="navbar-toggler navbar-dark" type="button" data-toggle="collapse" data-target="#navbar-dropdown"
          aria-controls="navbar-dropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon">
            <i class="fas fa-bars"></i>
          </span>
        </button>
      </div>
      <div class="collapse navbar-collapse" id="navbar-dropdown">
        <nav class="nav-primary">
          <div>
            <ul>
              @foreach (carbon_get_theme_option('layout-elements-menu-custom-links') as $item)
                <li class="nav-item">
                  <a class="nav-link" href="{{ $item['layout-elements-menu-custom-link-url'] }}">{{ $item['layout-elements-menu-custom-link-label'] }}</a>
                </li>
              @endforeach
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Pautas
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  @foreach ($guidelines as $guideline)
                    <a class="dropdown-item" href=" {{ get_permalink($guideline->ID) }} "> {{ $guideline->post_title }} </a>
                  @endforeach
                </div>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </nav>
   </div>
  </div>
  <form class="search-wrapper search-form">
    <div class="input-row">
      <div class="close-wrapper">
        <a href="#" class="close-button">
          <i class="fas fa-times"></i>
        </a>
      </div>
      @if(carbon_get_theme_option('social-medias-contact-whatsapp-show'))
        <a target="_blank" class="whatsapp d-lg-none" href="{{ $whatsapp_link }}">
          <i class="fab fa-whatsapp"></i>
        </a>
      @endif
      <input class="search-input" type="text" name="search-query" autocomplete="off" placeholder="Google da Diva. Pesquise...">
    </div>
    <div class="search-result">
    </div>
  </form>
</header>
