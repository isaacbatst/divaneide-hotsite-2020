<footer class="content-info">
  <div class="container">
    <div class="contact">
      <h3 class="title"> {{ carbon_get_theme_option('layout-elements-footer-contact-title') }} </h3>
      {!! do_shortcode('[contact-form-7 title="Form Rodapé"]') !!}
    </div>
  </div>
</footer>
