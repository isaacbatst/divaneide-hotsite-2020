<section name="main-content">
  <div class="container">
    <article>
      <div class="title-wrapper">
        <h1>{{ carbon_get_the_post_meta('template-interno-title') }}</h1>
        @if(get_the_terms( get_the_ID() , 'page-category'))
        <ul class="categories">
          @foreach(get_the_terms( get_the_ID() , 'page-category') as $category)
            @php $term_link = get_term_link( $category )
            @endphp
            <li class="list-inline-item">
              <a class="category" title="{{ $category->name }}" href="#">{{ $category->name }}</a>
            </li>
          @endforeach
        </ul>
        @endif
      </div>
      <div class="row">
        <div class="col-12 col-lg-6">
          <div class="text">
            {!! wpautop( carbon_get_the_post_meta( 'template-interno-text' ) ); !!}
          </div>
        </div>
        <div class="col-12 col-lg-6">
          <img class="img-fluid" src="{{ get_post(carbon_get_the_post_meta( 'template-interno-image' ))->guid }}">
        </div>
      </div>
      @include('partials.sections.template-interno.related-posts')
    </article>
  </div>
</section>
