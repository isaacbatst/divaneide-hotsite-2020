<section name="related-posts">
  <div class="container">
    @if(count($related_posts) > 0)
    <div class="related-posts">
      @foreach ($related_posts as $related_post)
        <a href="{{ get_permalink($related_post->ID) }}" target="_blank" class="related-post">
          <h4 class="title">{{ $related_post->post_title }}</h3>
          <div class="text">
            {!! wpautop( carbon_get_post_meta( $related_post->ID, $post_text_field ) ); !!}
          </div>
          <button class="see-more d-lg-none"> {{ $button_text }} </button>
          <div class="categories d-none d-lg-block">
            @if(get_the_terms( $related_post->ID , 'page-category'))
              <ul class="categories">
                @foreach(get_the_terms( $related_post->ID , 'page-category') as $category)
                @php $term_link = get_term_link( $category )
                @endphp
                <li class="list-inline-item">
                  <div class="category" title="{{ $category->name }}" href="#">{{ $category->name }}</div>
                </li>
                @endforeach
              </ul>
          @endif
          </div>
        </a>
      @endforeach
    </div>
  @endif
  </div>
</section>
