<section name="post-destaque"
  style="background-color: {{ carbon_get_the_post_meta('front-page-hard-news-background-color') }};
  color: {{ carbon_get_the_post_meta('front-page-hard-news-text-color') }};
  background-image: url('{{get_post(carbon_get_the_post_meta('front-page-hard-news-background-desktop'))->guid}}')">
  <div class="container">
    <div class="content-wrapper">
      <h1 data-aos="fade-right" class="title">{{ carbon_get_the_post_meta('front-page-hard-news-title') }}</h1>
      @include('partials.components.social-row')

      <div data-aos="fade-left" id="multi-item-example" class="carousel slide carousel-multi-item d-none d-lg-block" data-ride="carousel">

        <a class="carousel-control-prev" href="#multi-item-example" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#multi-item-example" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

        <div class="carousel-inner" role="listbox" style=" width:80vw; !important;">

          @for ($i = 0; $i < count(carbon_get_the_post_meta('front-page-hard-news-posts')); $i+=3)
          <div class="carousel-item {{ $i == 0 ? 'active' : '' }}">

            <div class="row">
              <div class="col-md-4">
                <a href='{{carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i]['id'],'cached-social-post-permalink')}}'>
                  @if (carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i]['id'],'cached-social-post-media-type') != "VIDEO" )
                  <object class="d-block" data="{{carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i]['id'],'cached-social-post-media-url')}}" type="image/png">
                    <img class="d-block" src="@asset('images/logo-vertical-white.png')" alt="Diva 13613">
                  </object>
                  @else
                  <object class="d-block" data="{{carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i]['id'],'cached-social-post-thumbnail-url')}}" type="image/png">
                    <img class="d-block" src="@asset('images/logo-vertical-white.png')" alt="Diva 13613">
                  </object>
                  @endif
                    <div class="bloco-i">
                      @switch(carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i]['id'],'cached-social-post-social-media'))
                          @case('instagram')
                          <i class="fab fa-instagram"></i>
                              @break
                          @case('facebook')
                          <i class="fab fa-facebook-f"></i>
                              @break
                          @case('twitter')
                          <i class="fab fa-twitter"></i>
                              @break
                          @default
                      @endswitch
                    </div>
                    <p>{!!carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i]['id'],'cached-social-post-caption')!!}</p>
                  </a>
              </div>

              @if (isset(carbon_get_the_post_meta('front-page-hard-news-posts')[$i + 1]))
              <div class="col-md-4 clearfix d-none d-md-block">
                <a href='{{carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i + 1]['id'],'cached-social-post-permalink')}}'>
                  @if (carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i + 1]['id'],'cached-social-post-media-type') != "VIDEO" )
                  <object class="d-block" data="{{carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i + 1]['id'],'cached-social-post-media-url')}}" type="image/png">
                    <img class="d-block" src="@asset('images/logo-vertical-white.png')" alt="Diva 13613">
                  </object>
                  @else
                  <object class="d-block" data="{{carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i + 1]['id'],'cached-social-post-thumbnail-url')}}" type="image/png">
                    <img class="d-block" src="@asset('images/logo-vertical-white.png')" alt="Diva 13613">
                  </object>
                  @endif
                    <div class="bloco-i">
                      @switch(carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i + 1]['id'],'cached-social-post-social-media'))
                          @case('instagram')
                          <i class="fab fa-instagram"></i>
                              @break
                          @case('facebook')
                          <i class="fab fa-facebook-f"></i>
                              @break
                          @case('twitter')
                          <i class="fab fa-twitter"></i>
                              @break
                          @default
                      @endswitch
                    </div>
                    <p>{!!carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i + 1]['id'],'cached-social-post-caption')!!}</p>
                  </a>
              </div>
              @endif

              @if (isset(carbon_get_the_post_meta('front-page-hard-news-posts')[$i + 2]))
              <div class="col-md-4 clearfix d-none d-md-block">
                <a href='{{carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i + 2]['id'],'cached-social-post-permalink')}}'>
                  @if (carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i + 2]['id'],'cached-social-post-media-type') != "VIDEO" )
                  <object class="d-block" data="{{carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i + 2]['id'],'cached-social-post-media-url')}}" type="image/png">
                    <img class="d-block" src="@asset('images/logo-vertical-white.png')" alt="Diva 13613">
                  </object>
                  @else
                  <object class="d-block" data="{{carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i + 2]['id'],'cached-social-post-thumbnail-url')}}" type="image/png">
                    <img class="d-block" src="@asset('images/logo-vertical-white.png')" alt="Diva 13613">
                  </object>
                  @endif
                    <div class="bloco-i">
                      @switch(carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i + 2]['id'],'cached-social-post-social-media'))
                          @case('instagram')
                          <i class="fab fa-instagram"></i>
                              @break
                          @case('facebook')
                          <i class="fab fa-facebook-f"></i>
                              @break
                          @case('twitter')
                          <i class="fab fa-twitter"></i>
                              @break
                          @default
                      @endswitch
                    </div>
                    <p>{!!carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i + 2]['id'],'cached-social-post-caption')!!}</p>
                  </a>
              </div>
              @endif
            </div>

          </div>
          @endfor
        </div>

      </div>

      <div data-aos="fade-up" id="multi-item-example" class="carousel slide carousel-multi-item d-lg-none" data-ride="carousel">

        <a class="carousel-control-prev" href="#multi-item-example" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#multi-item-example" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>

        <div class="carousel-inner" role="listbox" style=" width:80vw; !important;">

          @for ($i = 0; $i < count(carbon_get_the_post_meta('front-page-hard-news-posts')); $i++)
          <div class="carousel-item {{ $i == 0 ? 'active' : '' }}">

            <div class="row">
              <div class="col-md-4">
                <a href='{{carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i]['id'],'cached-social-post-permalink')}}'>
                  @if (carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i]['id'],'cached-social-post-media-type') != "VIDEO" )
                  <object class="d-block" data="{{carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i]['id'],'cached-social-post-media-url')}}" type="image/png">
                    <img class="d-block" src="@asset('images/logo-vertical-white.png')" alt="Diva 13613">
                  </object>
                  @else
                  <object class="d-block" data="{{carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i]['id'],'cached-social-post-thumbnail-url')}}" type="image/png">
                    <img class="d-block" src="@asset('images/logo-vertical-white.png')" alt="Diva 13613">
                  </object>
                  @endif
                    <div class="bloco-i">
                      @switch(carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i]['id'],'cached-social-post-social-media'))
                          @case('instagram')
                          <i class="fab fa-instagram"></i>
                              @break
                          @case('facebook')
                          <i class="fab fa-facebook-f"></i>
                              @break
                          @case('twitter')
                          <i class="fab fa-twitter"></i>
                              @break
                          @default
                      @endswitch
                    </div>
                    <p>{!!carbon_get_post_meta(carbon_get_the_post_meta('front-page-hard-news-posts')[$i]['id'],'cached-social-post-caption')!!}</p>
                  </a>
              </div>
            </div>

          </div>
          @endfor
        </div>

      </div>
    </div>
  </div>
</section>
