<section name="related-posts">
  <div class="container">
    @if(count($related_posts['internal']) > 0 || count($related_posts['external']) > 0)
      <div class="related-posts">
        @for ($i = 0; $i < 3 && $i < count($related_posts['internal']); $i++)
          @include('partials.sections.saiba-mais.internal-post', [
            'related_post' => $related_posts['internal'][$i]
          ])
        @endfor
        <div class="collaborative-platform-wrapper">
          @include('partials.sections.collaborative-platform')
        </div>
        @for ($i = 3; $i < count($related_posts['internal']); $i++)
          @include('partials.sections.saiba-mais.internal-post', [
            'related_post' => $related_posts['internal'][$i]
          ])
        @endfor
        @foreach ($related_posts['external'] as $related_post)
        @include('partials.sections.saiba-mais.external-post', [
          'related_post' => $related_post
        ])
      @endforeach
      </div>
    @else
      <div class="not-found d-flex justify-content-center">
        <span >Nós só estamos há 20 meses na Câmara Municipal de Natal! Essa não deu tempo de fazer... Deixe sua sugestão pra a gente!</span>
      </div>
    @endif
  </div>
</section>
