<section name="main-content">
  <div class="container">
    <article>
      <div class="title-wrapper row">
        <div class="col-12">
          <h1 class="title"> {{ carbon_get_theme_option($page_title_field) }} </h1>
          <h2>{{ carbon_get_the_post_meta($post_title_field) }}</h2>
          @if(get_the_terms( get_the_ID() , 'page-category'))
            <ul class="categories d-none d-lg-block">
              @foreach(get_the_terms( get_the_ID() , 'page-category') as $category)
              @php $term_link = get_term_link( $category )
              @endphp
              <li class="list-inline-item">
                <a class="category" title="{{ $category->name }}" href="#">{{ $category->name }}</a>
              </li>
              @endforeach
            </ul>
          @endif
        </div>
      </div>
      <div class="row d-flex">
        <div class="col-12 col-lg-8">
          <div class="text">
            {!! wpautop( carbon_get_the_post_meta( $post_text_field ) ); !!}
          </div>
        </div>
      </div>
    </article>
  </div>
</section>
