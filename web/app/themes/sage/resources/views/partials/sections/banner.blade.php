<section name="banner" id="{{ carbon_get_the_post_meta('front-page-banner-section-id') }}"
  style="background-color: {{ carbon_get_the_post_meta('front-page-banner-background-color') }};
  background-image: url('{{get_post(carbon_get_the_post_meta('front-page-banner-image-desktop'))->guid}}')">
  <div class="container">
    <div class="content-wrapper">
      <h1 class="title">{{ carbon_get_the_post_meta('front-page-banner-title') }}</h1>
      <div class="content-wrapper">
        <div class="row description-row">
          <div class="col-12 col-lg-8">
            <div class="description-wrapper">
              <img class="img-fluid d-lg-none" src="{{get_post(carbon_get_the_post_meta('front-page-banner-image-mobile'))->guid}}" alt="">
              <h2 class="description">{{ carbon_get_the_post_meta('front-page-banner-description') }}</h2>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
