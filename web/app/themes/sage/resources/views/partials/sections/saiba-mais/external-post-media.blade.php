@if($related_post['post_media_type'] === 'VIDEO' && $related_post['post_social_media'] !== 'facebook')
  <video class="img-fluid" src=" {{ $related_post['post_media_url'] }} " controls></video>
@else @if($related_post['post_media_type'] === 'VIDEO')
  <img class="img-fluid" src=" {{ $related_post['post_thumb_url'] }} " />
@endif
@endif

@if($related_post['post_media_type'] === 'IMAGE' || $related_post['post_media_type'] === 'CAROUSEL')
  <img class="img-fluid" src=" {{ $related_post['post_media_url'] }} " />
@endif
