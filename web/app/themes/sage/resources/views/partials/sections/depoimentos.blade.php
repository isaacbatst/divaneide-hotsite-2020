<section name="depoimentos"
  style="background-color: {{ carbon_get_the_post_meta('front-page-depoimentos-background-color') }};
  color: {{ carbon_get_the_post_meta('front-page-depoimentos-text-color') }};
  background-image: url('{{get_post(carbon_get_the_post_meta('front-page-depoimentos-background-desktop'))->guid}}')">
  <div class="container">
    <div class="content-wrapper">
      <h1 data-aos="fade-right" class="title">{{ carbon_get_the_post_meta('front-page-depoimentos-title') }}</h1>
      <div class="depoimentos">
        @foreach (carbon_get_the_post_meta( 'front-page-depoimentos-depoimentos' ) as $dep)
        <div class="depoimento">
          <img  data-aos="fade-up" class="img-fluid" src="{{get_post($dep['image'])->guid}}" alt="">
          <p data-aos="fade-up"  >{{ $dep['text'] }}</p>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</section>