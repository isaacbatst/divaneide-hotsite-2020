<article href="{{ get_permalink($related_post['ID']) }}" target="_blank"
  class="related-post internal {{ $related_post['post_type'] }}">
  <div class="d-flex flex-column justify-content-between">
    <h3 class="title"> {{ $related_post['post_title'] }} </h3>
    <div class="text">
      {!! substr(wpautop( $related_post['post_text'] ), 0, 250).'...' !!}
    </div>
    <div class="post-footer d-flex justify-content-between">
      <a class="post-link" target="_blank" href="{{ $related_post['permalink']}}">
        Ver mais
      </a>
      @if($related_post['post_type'] === 'proposal')
        <h4 class="post-type">#Propostas</h4>
      @endif
      @if($related_post['post_type'] === 'already-done')
        <h4 class="post-type">#DivaFez</h4>
      @endif
      @if($related_post['post_type'] === 'page')
        <h4 class="post-type">#Pautas</h4>
      @endif
    </div>

  </div>
</article>
