<section name="related-posts">
  <div class="row d-flex justify-content-center">
    @if($related_proposals->have_posts())
      <div class="col-12 col-lg-6">
        <div class="posts-list proposals">
          <h3 class="title">Propostas</h3>
          <div class="row">
            @while ($related_proposals->have_posts())
              @php $related_proposals->the_post() @endphp
              <div class="col-12">
                <a href="{{ get_permalink() }}" class="related-post row no-gutters">
                  {!! get_the_title() !!}
                  <div class="tag">
                    Ver proposta
                  </div>
                </a>
              </div>
            @endwhile
            @php wp_reset_query() @endphp
          </div>
        </div>
      </div>
    @endif
    @if($related_already_done->have_posts())
      <div class="col-12 col-lg-6">
        <div class="posts-list already-done">
          <h3 class="title">#DivaFez</h3>
          <div class="row">
            @while ($related_already_done->have_posts())
              @php $related_already_done->the_post() @endphp
              <div class="col-12">
                <a href="{{ get_permalink() }}" class="related-post row no-gutters">
                  {!! get_the_title() !!}
                  <div class="tag">
                    Ver #DivaFez
                  </div>
                </a>
              </div>
            @endwhile
            @php wp_reset_query() @endphp
          </div>
        </div>
      </div>
    @endif
  </div>
</section>
