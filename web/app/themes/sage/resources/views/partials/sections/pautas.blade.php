<section name="pautas"
  style="background-color: {{ carbon_get_the_post_meta('front-page-pautas-background-color') }};
  color: {{ carbon_get_the_post_meta('front-page-pautas-text-color') }};
  background-image: url('{{get_post(carbon_get_the_post_meta('front-page-pautas-background-desktop'))->guid}}')">
  <div class="container">
    <div class="content-wrapper">
      <h1 data-aos="fade-right" class="title">{{ carbon_get_the_post_meta('front-page-pautas-title') }}</h1>
      <div data-aos="fade-left" class="pautas d-none d-lg-flex">
        @foreach (carbon_get_the_post_meta( 'front-page-pautas-pautas' ) as $pauta)
          <a href='{{ get_permalink($pauta['id']) }}'>{{ carbon_get_post_meta($pauta['id'], 'template-interno-title') }}</a>
        @endforeach
      </div>
      <div data-aos="fade-up" class="pautas d-lg-none">
        @foreach (carbon_get_the_post_meta( 'front-page-pautas-pautas' ) as $pauta)
          <a href='{{ get_permalink($pauta['id']) }}'>{{ carbon_get_post_meta($pauta['id'], 'template-interno-title') }}</a>
        @endforeach
      </div>
    </div>
  </div>
</section>