<section name="main-content">
  <div class="container">
    <h1 class="title"> {{ carbon_get_the_post_meta('template-downloads-title') }} </h1>
    <div class="downloads">
      @foreach (carbon_get_theme_option('layout-elements-downloads') as $item)
        <a href="{{ get_post($item['layout-elements-downloadable-file'])->guid }}">
          {{ $item['layout-elements-downloadable-name'] }}
        </a>
    @endforeach
    </div>
  </div>
</section>
