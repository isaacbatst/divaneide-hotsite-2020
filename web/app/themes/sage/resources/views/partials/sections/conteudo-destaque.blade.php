<section name="conteudo-destaque"
  style="background-color: {{ carbon_get_the_post_meta('front-page-spotlight-content-background-color') }}; 
  color: {{ carbon_get_the_post_meta('front-page-spotlight-content-text-color') }}; 
  background-image: url('{{get_post(carbon_get_the_post_meta('front-page-spotlight-content-background-desktop'))->guid}}')">
  <div class="container d-none d-lg-flex">
    <div data-aos="fade-right" class="content-wrapper">
      <h1>{{ carbon_get_the_post_meta('front-page-spotlight-content-title') }}</h1>
      <div class="text">{!! wpautop(carbon_get_the_post_meta('front-page-spotlight-content-text')) !!}</div>
      @if (carbon_get_the_post_meta('front-page-spotlight-content-button-show'))
        <a href='{{get_post(carbon_get_the_post_meta('front-page-spotlight-content-button-file'))->guid}}'>{{carbon_get_the_post_meta('front-page-spotlight-content-button-text')}}</a>
      @endif
    </div>
    <div data-aos="fade-left" class="media-warpper">
      @if (carbon_get_the_post_meta('front-page-spotlight-content-content-type') == 'video')
        <iframe width="420" height="315"
          src='https://www.youtube.com/embed/{{carbon_get_the_post_meta('front-page-spotlight-content-video-url')}}'>
        </iframe>
      @else
        <img src="{{get_post(carbon_get_the_post_meta('front-page-spotlight-content-img'))->guid}}" alt="">
      @endif
    </div>
  </div>
  <div data-aos="fade-up" class="container d-lg-none">
    <div class="content-wrapper">
      <h1>{{carbon_get_the_post_meta('front-page-spotlight-content-title')}}</h1>
      <div class="text">{!! wpautop(carbon_get_the_post_meta('front-page-spotlight-content-text')) !!}</div>
      <div class="media-warpper">
        @if (carbon_get_the_post_meta('front-page-spotlight-content-content-type') == 'video')
          <iframe width="420" height="315"
            src='https://www.youtube.com/embed/{{carbon_get_the_post_meta('front-page-spotlight-content-video-url')}}'>
          </iframe>
        @else
          <img src="{{get_post(carbon_get_the_post_meta('front-page-spotlight-content-img'))->guid}}" alt="">
        @endif
      </div>
      @if (carbon_get_the_post_meta('front-page-spotlight-content-button-show'))
        <a href='{{get_post(carbon_get_the_post_meta('front-page-spotlight-content-button-file'))->guid}}'>{{carbon_get_the_post_meta('front-page-spotlight-content-button-text')}}</a>
      @endif
    </div>
  </div>
</section>
