<section name="collaborative-platform">
  <div class="container">
    <h2 class="title text-center"> {{ carbon_get_post_meta(get_option('page_on_front'), 'front-page-cata-lead-title') }} </h2>
    <div class="row d-flex justify-content-center">
      <div class="col-12 col-lg-8">
        <div class="form-wrapper">
          <h3 class="subtitle">{{ carbon_get_post_meta(get_option('page_on_front'), 'front-page-cata-lead-subtitle') }}</h3>
          {!! do_shortcode('[contact-form-7 title="Cata Lead"]') !!}
          <p class="legal-text"> {{ carbon_get_post_meta(get_option('page_on_front'), 'front-page-cata-lead-legal-text') }} </p>
        </div>
      </div>
    </div>
  </div>
</section>
