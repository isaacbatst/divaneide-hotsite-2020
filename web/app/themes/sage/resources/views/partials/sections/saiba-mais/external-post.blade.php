<article class="related-post external {{ $related_post['post_social_media'] }}">
  <div class="row">
    <div class="col-12 col-lg-4">
      <div class="media-wrapper">
        @include('partials.sections.saiba-mais.external-post-media', [
          'related_post' => $related_post
        ])
      </div>
    </div>
    <div class="col-12 col-lg-8 d-flex flex-column justify-content-between">
      <div class="text">
        {{ substr($related_post['post_caption'], 0, 250).'...' }}
      </div>
      <div class="post-footer">
        <a class="post-link" target="_blank"  href="{{ $related_post['post_permalink']}}" >
          <i class="fab fa-{{ $related_post['post_social_media'] }}" ></i>
          Ver no @divaneide.basilio
        </a>
        <time datetime="{{$related_post['post_date']}}" pubdate>
          Postado em {{ date_format(date_create($related_post['post_date']), 'd/m/Y') }}
        </time>
      </div>
    </div>

  </div>

  <div class="categories d-none d-lg-block">
    @if(get_the_terms( $related_post['ID'] , 'page-category'))
    <ul class="categories">
      @foreach(get_the_terms( $related_post['ID'] , 'page-category') as $category)
      @php $term_link = get_term_link( $category )
      @endphp
      <li class="list-inline-item">
        <div class="category" title="{{ $category->name }}" href="#">{{ $category->name }}</div>
      </li>
      @endforeach
    </ul>
    @endif
  </div>
</article>
