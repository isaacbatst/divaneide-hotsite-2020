<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.blocks.head')
  <body @php body_class() @endphp>
    @include('partials.components.google-tag-manager')
    @if (carbon_get_theme_option('layout-elements-is-stand-by'))
      <div class="stand-by">
        <h1>{{carbon_get_theme_option('layout-elements-text-stand-by')}}</h1>
      </div>
    @else
      @php do_action('get_header') @endphp
      @include('partials.blocks.header')
      <div class="wrap" role="document">
        <div class="content">
          <main class="main">
            @yield('content')
          </main>
        </div>
      </div>
      @php do_action('get_footer') @endphp
      @include('partials.blocks.footer')
      @include('partials.components.social-bar')
      @php wp_footer() @endphp
    @endif
  </body>
</html>
