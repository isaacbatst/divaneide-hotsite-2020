@extends('layouts.app')

@section('content')
  @include('partials.sections.internal-main-content', [
    'page_title_field' => 'layout-elements-proposals-title',
    'post_title_field' => 'proposal-title',
    'post_text_field' => 'proposal-text',
  ])
  @include('partials.sections.internal-related-posts', [
    'button_text' => 'Ver propostas relacionadas',
    'post_text_field' => 'proposal-text'
  ])
@endsection
