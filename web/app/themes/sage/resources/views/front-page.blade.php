@extends('layouts.app')

@section('content')
  @include('partials.sections.banner')
  @include('partials.sections.collaborative-platform')
  @include('partials.sections.post-destaque')
  @include('partials.sections.conteudo-destaque')
  @include('partials.sections.depoimentos')
@endsection
