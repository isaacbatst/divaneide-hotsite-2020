@extends('layouts.app')

@section('content')
  @include('partials.sections.internal-main-content', [
    'page_title_field' => 'layout-elements-already-done-title',
    'post_title_field' => 'already-done-title',
    'post_text_field' => 'already-done-text',
  ])
  @include('partials.sections.internal-related-posts', [
    'button_text' => 'Ver trabalhos relacionados',
    'post_text_field' => 'proposal-text'
  ])
@endsection
