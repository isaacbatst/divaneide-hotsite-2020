import $ from 'jquery';
import 'jquery-mask-plugin';

export function whatsapp() {
  $('input[name="whatsapp"]').mask('(00) 00000-0000');
}
