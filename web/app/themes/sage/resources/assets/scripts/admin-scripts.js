/* eslint-disable no-undef */
import socialMediaThemeOption from './routes/admin/socialMediaThemeOption';
import Router from './util/Router';
import postType from './routes/admin/postType';

const routes = new Router({
  toplevelPageCrbCarbonFieldsContainerRedesSociais: socialMediaThemeOption,
  postTypePage: postType,
  postTypeCachedSocialPost: postType,
})

jQuery(document).ready(() => routes.loadEvents());
