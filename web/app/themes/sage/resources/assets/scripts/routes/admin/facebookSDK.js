/* eslint-disable no-undef */

import { insertButtonsContainerIntoHtmlHook as insertButtonsContainerIntoPostType } from './postType';
import { insertAlertIntoHtmlHook as insertAlertIntoPostType } from './postType';
import { insertButtonsContainerIntoHtmlHook as insertButtonsContainerIntoSocialMediaThemeOption } from './socialMediaThemeOption';

export default function init() {
  (function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  window.fbAsyncInit = function () {
    FB.init({
      appId: '243673650328260',
      cookie: true,
      xfbml: true,
      version: 'v8.0',
      autoLogAppEvents: true,
    });

    FB.AppEvents.logPageView();

    const insertIntoPage = getInsertButtonsIntoPageCallback();

    FB.getLoginStatus(response => {
      handleStatus(response, insertIntoPage)
    });
  };

  window.checkLoginState = function () {
    FB.getLoginStatus(response => {
      const insertIntoPage = getInsertButtonsIntoPageCallback();
      handleStatusChange(response, insertIntoPage)
    });
  }
  function handleStatus(response, insertIntoPage) {
    console.log(response)
    insertFacebookContainerIntoPage(insertIntoPage)

    if (response.status === 'connected') {
      removeLoginButton();
      handleLoggedData(response);
    } else {
      renderLoginButton();
    }

  }

  function handleStatusChange(response, insertIntoPage) {
    handleStatus(response, insertIntoPage);

    if (response.status === 'connected') {
      insertAlertIntoPage();
    }
  }

  function insertFacebookContainerIntoPage(insertIntoPage) {
    const facebookContainer = document.createElement('div');
    facebookContainer.classList.add('facebook-container');
    insertIntoPage(facebookContainer);
  }


  function saveUpdatedAccessToken(accessToken) {
    fetch('/wp/wp-admin/admin-ajax.php?action=save_facebook_access_token', {
      method: 'POST',
      body: JSON.stringify({
        accessToken: accessToken,
      }),
    })
  }

  function handleLoggedData(response) {
    insertLoggedDataIntoPage();
    saveUpdatedAccessToken(response.authResponse.accessToken)
  }

  function insertLoggedDataIntoPage() {
    FB.api('/me', response => {
      const facebookContainer = document.querySelector('.facebook-container');
      const logoutButton = createLogoutButton(response.name);
      facebookContainer.innerHTML = '';
      facebookContainer.appendChild(logoutButton);
    })
  }

  function createLogoutButton(name) {
    const logoutButton = document.createElement('button');
    logoutButton.className = 'facebook social-auth-button';
    logoutButton.innerText = `Logado como: ${name}, deslogar?`
    logoutButton.addEventListener('click', () => {
      FB.logout();
      deleteAccessToken();
      removeLoginButton();
      renderLoginButton();
    })

    return logoutButton;
  }

  function deleteAccessToken() {
    const adminAjaxUrl = '/wp/wp-admin/admin-ajax.php';

    return fetch(`${adminAjaxUrl}?action=delete_facebook_access_token`, {
      method: 'DELETE',
    })
  }

  function removeLoginButton() {
    const facebookContainer = document.querySelector('.facebook-container');

    if (facebookContainer) {
      facebookContainer.innerHTML = '';
    }
  }

  function renderLoginButton() {
    const facebookContainer = document.querySelector('.facebook-container');

    if (facebookContainer) {
      const facebookButtonHTML =
        '<div  class="fb-login-button" \
        onlogin="checkLoginState();" data-scope="user_posts, user_videos" \
        data-size="large" data-button-type="login_with" data-layout="default" \
        data-auto-logout-link="false" data-use-continue-as="false" data-width="" >\
        Autorizar facebook\
      </div>';

      facebookContainer.innerHTML = facebookButtonHTML;
    }
  }

  function insertAlertIntoPage() {
    const body = document.querySelector('body');

    const successAlert = document.createElement('div');
    successAlert.className = 'alert success';
    successAlert.innerText = 'Login realizado!';

    if (body.classList.contains('post-type-cached-social-post') || body.classList.contains('post-type-page')) {
      return insertAlertIntoPostType(successAlert);
    }

    if (body.classList.contains('toplevel_page_crb_carbon_fields_container_redes_sociais')) {
      return insertButtonsContainerIntoSocialMediaThemeOption(successAlert);
    }

    return () => { };
  }
}

function getInsertButtonsIntoPageCallback() {
  const body = document.querySelector('body');

  if (body.classList.contains('post-type-cached-social-post') || body.classList.contains('post-type-page')) {
    return insertButtonsContainerIntoPostType;
  }

  if (body.classList.contains('toplevel_page_crb_carbon_fields_container_redes_sociais')) {
    return insertButtonsContainerIntoSocialMediaThemeOption;
  }

  return () => { };
}
