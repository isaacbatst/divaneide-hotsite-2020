import AOS from 'aos/dist/aos';
import { whatsapp as maskWhatsapp } from '../util/mask';

import 'bootstrap';

export default {
  init() {
    // JavaScript to be fired on the home page
    AOS.init({duration:1400})
    AOS.refresh()
    AOS.refreshHard();

    maskWhatsapp();
    closeDropdownHandler();
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};

function closeDropdownHandler() {
  document.addEventListener('click', event => {
    if(event.target.closest('header') === null) {
      $('#navbar-dropdown').collapse('hide')
    }
  })
}
