import header from '../components/header/';

export default {
  init() {
    // JavaScript to be fired on all pages
    header();
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
