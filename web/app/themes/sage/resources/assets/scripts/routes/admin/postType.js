import socialMediasLogin from './socialMediasLogin';

export default {
  init() {
    socialMediasLogin.attach(insertButtonsContainerIntoHtmlHook);
  },
}

export function insertButtonsContainerIntoHtmlHook(alertContainer) {
  const postStuff = document.getElementById('poststuff');
  const carbonBox = postStuff ? document.querySelector('.postbox.carbon-box') : undefined;

  if (postStuff && carbonBox) {
    return carbonBox.insertAdjacentElement('beforebegin', alertContainer);
  }

  document.querySelector('.wp-list-table ').insertAdjacentElement('beforebegin', alertContainer)
}
export function insertAlertIntoHtmlHook(alertContainer) {
  const postStuff = document.getElementById('poststuff');
  const socialButtonsContainer = postStuff ? postStuff.querySelector('.social-auth-buttons-container') : undefined;

  if (postStuff && socialButtonsContainer) {
    return socialButtonsContainer.insertAdjacentElement('beforebegin', alertContainer);
  }

  document.querySelector('#wpbody-content .wrap').prepend(alertContainer)
}

