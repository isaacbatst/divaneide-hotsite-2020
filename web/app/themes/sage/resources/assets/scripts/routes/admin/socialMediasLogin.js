export default {
  attach(insertIntoPage) {
    requestAccessTokenValidation()
      .then(response => {
        createButtonsIfSocialMediaIsNotAuthenticated(response, insertIntoPage)
      })
  },
}

const buttons = {
  instagram: {
    textContent: 'Autorizar Instagram',
    class: 'instagram',
    ajaxAction: 'authorize_instagram',
  },
  facebook: {
    textContent: 'Autorizar Twitter',
    class: 'twitter',
    ajaxAction: 'authorize_twitter',
    disabled: true,
  },
};


const adminAjaxUrl = '/wp/wp-admin/admin-ajax.php';

function createButtonsIfSocialMediaIsNotAuthenticated(socialMedias, insertIntoPage) {

  Object.entries(socialMedias).reduce((containerCreated, [socialMedia, isAuthenticated]) => {
    if (!isAuthenticated) {
      containerCreated ? 0 : createButtonsContainer(insertIntoPage);
      createButton(buttons[socialMedia])

      return true;
    }

    return false
  }, false)
}

function createButton(button) {
  const element = document.createElement('button');

  element.textContent = button.textContent
  element.classList.add(button.class, 'social-auth-button');

  if(button.disabled) {
    element.setAttribute('disabled', true);
  }

  element.addEventListener('click', event => {
    event.preventDefault();
    window.location.href = `${adminAjaxUrl}?action=${button.ajaxAction}&location=${window.location.href}`
  })

  document.querySelector('.social-auth-buttons-container').append(element)
}

function createButtonsContainer(insertIntoPage) {
  const buttonsContainer = document.createElement('div');
  buttonsContainer.classList.add('social-auth-buttons-container')

  insertIntoPage(buttonsContainer);

  return buttonsContainer;
}



function requestAccessTokenValidation() {
  return fetch('/wp/wp-admin/admin-ajax.php?action=are_social_medias_authenticated')
    .then(response => {
      return response.json()
    })
}
