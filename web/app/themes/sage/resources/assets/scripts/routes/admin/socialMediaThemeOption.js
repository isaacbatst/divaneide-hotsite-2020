import socialMediasLogin from './socialMediasLogin';

export default {
  init() {
    socialMediasLogin.attach(insertButtonsContainerIntoHtmlHook);
  },
}

export function insertButtonsContainerIntoHtmlHook(socialMediasButtonsContainer) {
  const container = document.getElementById('post-body-content');
  container.prepend(socialMediasButtonsContainer);
}
