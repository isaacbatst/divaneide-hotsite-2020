import handlePostSearch from './postSearch';
import handleScroll from './scroll';

export default () => {
  handlePostSearch();
  handleScroll();
}
