import $ from 'jquery';

const searchInput = document.querySelector('.search-wrapper input');
const searchForm = document.querySelector('.search-wrapper.search-form');
const searchResult = document.querySelector('.search-wrapper .search-result');
const closeButtonWrapper = document.querySelector('.search-wrapper .close-wrapper');

const adminAjaxUrl = '/wp/wp-admin/admin-ajax.php';

export default () => {
  handleSearchFormSubmit();
  handleSearchResultVisibility();
  handleCloseButton();
}

function resetSearch() {
  $(searchResult).fadeOut(100);
  $(closeButtonWrapper).fadeOut(100);
  searchInput.value = '';
}

function handleCloseButton() {
  const closeButton = closeButtonWrapper.querySelector('a');

  closeButton.addEventListener('click', event => {
    event.preventDefault();
    resetSearch();
  })
}

function handleSearchFormSubmit() {
  searchForm.addEventListener('submit', event => {
    event.preventDefault();

    const query = searchInput.value;

    const queryParamsObject = {
      action: 'search_posts',
      query,
    }

    const queryParamsUrlFormatted = new URLSearchParams(queryParamsObject).toString();

    searchResult.innerHTML = '<i class="fas fa-spinner fa-spin"></i>';
    $(searchResult).fadeIn(300);

    fetch(`${adminAjaxUrl}?${queryParamsUrlFormatted}`)
      .then(response => response.json())
      .then(response => {
        if (response.matchingPosts) {
          renderSearchResult(response.matchingPosts);
          $(closeButtonWrapper).fadeIn();
        }
      })
      .catch(error => {
        renderNoResult(error);
      })
  })
}

function renderNoResult(error) {
  searchResult.innerHTML = `<div class="not-found">${error.message}</div>`;
}

function renderSearchResult(matchingPosts) {
  const internalPosts = matchingPosts.internal;
  const externalPosts = matchingPosts.external;

  if ((!internalPosts && !externalPosts) || (internalPosts.length === 0 && externalPosts.length === 0)) {
    throw new Error('Nós só estamos há 20 meses na Câmara Municipal de Natal! Essa não deu tempo de fazer... Deixe sua sugestão pra a gente!');
  }

  searchResult.innerHTML = '<span class="label">Resultados</span>';

  const postsElements = document.createElement('div');
  postsElements.className = 'posts-wrapper';
  searchResult.appendChild(postsElements)

  if (internalPosts) {
    renderInternalPosts(internalPosts);
  }

  if (externalPosts) {
    renderExternalPosts(externalPosts);
  }

  renderKnowMoreAbout()
}

function renderKnowMoreAbout() {
  const element = document.createElement('div');
  element.className = 'know-more';
  element.innerHTML = `<a class="link" href="/saiba-mais?query=${searchInput.value}">Clique aqui para saber mais sobre o tema!</a>`

  searchResult.appendChild(element);
}

function renderExternalPosts(externalPosts) {
  const postsElements = document.querySelector('.posts-wrapper');

  postsElements.innerHTML += externalPosts.reduce((elements, post) => {
    const socialIcon = getSocialIcon(post.post_social_media);
    const title = getPostTitle(post.post_caption);
    const image = getThumb(post);

    return `${elements}
    <a class="external-post post" target="_blank" href="${post.post_permalink}">
      <div class="img-wrapper">${image}</div>
      <h3> ${title} </h3>
      <div class="social-icon-wrapper">${socialIcon}</div>
    </a>`
  }, '');

  searchResult.appendChild(postsElements);
}

function getPostTitle(caption) {
  if (caption.length > 50) {
    return `${caption.substring(0, 50)}...`;
  }

  return caption;
}

function getThumb(post) {
  if (post.post_media_type === 'IMAGE') {
    return getImageThumb(post.post_media_url);
  }

  if (post.post_media_type === 'VIDEO') {
    return getVideoThumb(post.post_thumb_url)
  }

  return '<i class="fas fa-image"></i>';
}

function getImageThumb(mediaUrl) {
  if (mediaUrl === '') {
    return '<i class="fas fa-image"></i>';
  }

  return `<img class="img-fluid" src=${mediaUrl} />`
}

function getVideoThumb(mediaUrl) {
  if (mediaUrl === '') {
    return '<i class="fas fa-image"></i>';
  }

  return `<img class="img-fluid" src=${mediaUrl} />`
}

function getSocialIcon(socialMedia) {
  if (socialMedia === 'facebook') {
    return '<i class="fab fa-facebook-f social-icon facebook"></i>';
  }

  if (socialMedia === 'instagram') {
    return '<i class="fab fa-instagram social-icon instagram"></i>'
  }

  return '';
}
function renderInternalPosts(internalPosts) {
  const postsElements = document.querySelector('.posts-wrapper');

  const internalPostsFiltered = internalPosts
    .reduce((posts, post) => {
      if (noneInternalPostFromType(posts, post.post_type)) {
        return [
          ...posts,
          post,
        ]
      }

      return posts;
    }, [])

  postsElements.innerHTML += internalPostsFiltered.reduce((elements, post) => {
    return `${elements}
    <a class="internal-post post ${post.post_type}" target="_blank" href="${post.permalink}">
      <div class="img-wrapper">${getImageThumb(post.post_image)}</div>
      <h3>${post.post_title}</h3>
      <div class="social-icon-wrapper"><i class="fas fa-star social-icon"></i></div>
    </a>`
  }, '');
}

function noneInternalPostFromType(posts, type) {
  return !posts.some(post => post.post_type === type);
}

function handleSearchResultVisibility() {
  const body = document.querySelector('body');

  body.addEventListener('click', event => {
    if (event.target.classList.contains('search-wrapper') || event.target.closest('.search-wrapper')) {
      return;
    }

    resetSearch();
  })
}
