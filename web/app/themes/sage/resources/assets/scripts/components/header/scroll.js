import logoVerticalWhite from '../../../images/logo-vertical-white.png';
import logoVerticalBlack from '../../../images/logo-vertical-black.png';
export default () => {

  const isFrontPage = document.querySelector('body').classList.contains('home');

  window.onscroll = function () {
    if (window.innerWidth > 998 && isFrontPage) {
      handleDesktopScrolling();
    }
  }

  function handleDesktopScrolling() {
    if (window.scrollY > 50) {
      return handleDesktopBellowTop();
    }

    return handleDesktopOnTop();
  }

  function handleDesktopBellowTop() {
    document.getElementsByTagName('header').item(0).classList.add('invert');

    const brandImage = document.querySelector('header .brand img');
    brandImage.setAttribute('src', logoVerticalBlack)
  }

  function handleDesktopOnTop() {
    document.getElementsByTagName('header').item(0).classList.remove('invert')
    const brandImage = document.querySelector('header .brand img');
    brandImage.setAttribute('src', logoVerticalWhite)
  }
}
